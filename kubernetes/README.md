# Tour de Planet - Kubernetes Config

This folder holds the configuration files for using the whole application stack in kubernetes:

## Tour de Planet Staging environment

- reachable over [staging.tourdeplanet.org](staging.tourdeplanet.org)
- `service.staging.yaml` and `deployment.staging.yaml` define service, deployment to use with Routing in `ingress.yaml`
- - for updates run `kubectl apply -f [deploy|service].staging.yaml

## Tour de Planet Production environment

- reachable over [tourdeplanet.org](tourdeplanet.org) (and many more)
- `service.production.yaml` and `deployment.production.yaml` define service, deployment to use with Routing in `ingress.yaml`
- for updates run `kubectl apply -f [deploy|service].production.yaml

## Ingress and Certificate

`certificates.yaml` holds the cofnig to enable certbot certification in our k8s. You can find each host and each Route pointing to our prod/staging in `ingress.yaml`. So when adding new domain pointing to our staging/production app, you should add it into `ingress.yaml` and run `kubectl apply -f ingress.yaml` and into `certificates.yaml` and run `kubectl apply -f certificates.yaml`

## Helm values for stable/mysql Chart

For the CMS we use MySQL as database for both environments. For updates run:

```bash
helm upgrade --name tourderebel --namespace tourderebel --values helm_values.yaml stable/mysql
```
## Security

We use one secret to setup secret values used in the applications. So to change those values, you should run `kubectl apply -f secrets.yaml`. But hold on ..., this file does not exist? Yes... We won't persist secret values like database credentials in our code repository. So we have to build our secrets.yaml file by running:

```bash

cp .env.dist .env

# change ALL values to not override some by yours

make secrets.yaml

kubectl apply -f secrets.yaml
```

(same for `postgress_helm_chart.yaml` and `helm_values.yaml`)

Please be aware that the credentials will be changed imediatly, but inside your pods, the values will be available after a rebuild only.
