#!/bin/bash

shift $(($OPTIND - 1))
version=$2
type=$1

if [ "${type}" == "patch" ]; then
  patch=true;
elif [ "${type}" == "minor" ]; then
  minor=true
elif [ "${type}" == "major" ]; then
  major=true
fi;

# Build array from version string.
a=( ${version//./ } )

# If version string is missing or has the wrong number of members, show usage message.

if [ ${#a[@]} -ne 3 ]
then
  echo "usage: $(basename $0) [patch|minor|major] major.minor.patch"
  exit 1
fi

# Increment version numbers as requested.

if [ ! -z $major ]
then
  ((a[0]++))
  a[1]=0
  a[2]=0
fi

if [ ! -z $minor ]
then
  ((a[1]++))
  a[2]=0
fi

if [ ! -z $patch ]
then
  ((a[2]++))
fi

echo "${a[0]}.${a[1]}.${a[2]}"
