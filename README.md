# Developers For Future - Tour de Planet Website

Currently it is a Symfony based application called [Sulu.io](https://sulu.io) with some help of Symfony CMF. To run the app locally run a:

```bash
# Clone the repository
git clone git@gitlab.com:developersforfuture/tour-de-planet.git

cd tour-de-planet

# edit it for your local usage
export $(egrep -v '^#' .env | xargs)

docker-compose -f docker-compose.development.yaml up -d
```

#d# Run completely in docker-compose setup

Start docker containers from within `docker-compose.yaml`. So we start it by calling:

```bash
docker-compose up -d
```

Usualy you have build the content store for the first time, so jump into the container by `docker-compose exec tourderebel-app sh`. There you run the following commands ind Startup Application`  section.
Now you shoud be able to open [http://0.0.0.0:7082[(http://0.0.0.0:7082)` and see an empty homepage (needs to be published in admin first)

To reach admin ui jsut go to [http://0.0.0.0:7082/admin](http://0.0.0.0:7082/admin). User and Password is `admin` and `admin` in dev mode

### Run on lokal PHP server

Havin installed a php locally, you can run the app itself locally. Therefore you have to build up your environment variables. By
```bash
#### cCopy from dist version
cp .env.dist .env
```
setup your own vars. most of them can stay, When using mysql and minio from dockeer-compose spin them up by:

``` bash
docker-compose up -d
``` 

then the credentials in your copied .env file should work.But you can also use your owwn database wherever you want to. Just setup credentials in the `.env`file. You read them in by:

```bash
export $(egrep -v '^#' .env | xargs)
```

Now jump into the app folder and run:

```bash
cd app/src
bin/console servere:run
```

usually you can vistt [http://127.0.0.1:80000]([http://127.0.0.1:80000)  now and also use hthe admin [[http://127.0.0.1:80000/admin]([http://127.0.0.1:80000/admin)


### Start Application

``` bash
bin/console sulu:build dev
bin/console cache:clear
bin/websiteconsole cache:clear

# having rights issues complete it by:
setfacl -R -m u:"${SYSTEM_APPUSER_NAME}":rwX -m u:${SYSTEM_APPUSER_NAME}:rwX var public/uploads
setfacl -dR -m u:"${SYSTEM_APPUSER_NAME}":rwX -m u:${SYSTEM_APPUSER_NAME}:rwX var public/uploads
```

### Assets by Webpack Encore

As the assets are handled by webpack, you should run its whatcher when working with Sass or Typescript files. So:

```bash
cd app/src
npm run watch
```

*hint:* working within the docker container will serve it automatically

#### Admin UI

The code running the admin got a webpack also:

```bash
cd app/src/assets/admin/
npm install
npm run build
```

Doing so, you will always get the changes.
s