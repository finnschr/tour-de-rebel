import 'bootstrap';
import 'jquery';
import './cookies';


$('a[data-id="newsletter-mail"]').on('click', function () {
    var dataId = $(this).data('id');
    var token = $(this).data('token');
    var mailInput = $('input#' + dataId);
    var url = $(this).data('url');
    if (!mailInput) {
        return;
    }

    var emailAddress = mailInput.val();
    if (emailAddress.length === 0) {
        alert('Insert Data before submit smth');
        return;
    }

    $.post(url, {token, emailAddress}
    ).done(function (response) {
        mailInput.removeClass('is-invalid').addClass('is-valid');
        $('#newsletterHelp').html(response['success']).removeClass('text-danger').addClass('text-success').show();

        return;

    }).fail(function (response) {
        mailInput.addClass('is-invalid').removeClass('is-valid');
        console.log(response);
        var errorMsg = (response.hasOwnProperty('responseJSON')) && response.responseJSON.hasOwnProperty('error') ? response.responseJSON['error'] : 'Tschüs';
        $('#newsletterHelp').html(errorMsg).removeClass('text-success').addClass('text-danger').show();

        return;
    });
});

// Main nav toggle / close functionality
$(function () {
    // The open / close state of the main nav is controlled primarily with the
    // aria-expanded property. By doing this through an aria-* property we
    // make the visible state reflect the aria state and thereby ensure that
    // these two are aligned.
    // However, in order for the CSS transitions to work, we're also setting
    // a property on the body element.
    var navMainBtn = $('#nav-main-btn');
    var body = $(document.body);

    function isNavOpen() {
        return navMainBtn.attr('aria-expanded') === 'true';
    }

    function setNavOpenState(open) {
        if (open) {
            navMainBtn.attr('aria-expanded', 'true');
            window.setTimeout(function () {
                body.attr('data-nav-open', 'true');
            }, 40);
        } else {
            body.attr('data-nav-open', 'false');
            window.setTimeout(function () {
                navMainBtn.attr('aria-expanded', 'false');
            }, 600);
        }
    }

    function toggleNavOpenState() {
        setNavOpenState(!isNavOpen());
    }

    setNavOpenState(false);
    navMainBtn.on('click', toggleNavOpenState);

    // When the nav is open and you press ESC close it
    $(document).on('keyup', function onKeyUp(e) {
        var isEscKeyReleased = e.key === 'Escape';
        if (isNavOpen() && isEscKeyReleased) {
            // Close navigation
            setNavOpenState(false);
        }
    })
})