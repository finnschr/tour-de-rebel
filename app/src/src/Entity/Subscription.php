<?php declare(strict_types = 1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 * @ORM\Entity()
 */
class Subscription
{
    const RESOURCE_KEY = 'subscriptions';
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Email Address
     *
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $emailAddress;

    /**
     * By using this form you agree with the storage and handling of your data by this website in accordance with our Privacy Policy. (*, checkbox)
     *
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $isDataPolicyAccepted = false;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $isVerified = false;
    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $verificationToken;

    /**
     * @var \DateTime $created
     *
     * @ORM\Column(type="datetime")
     */
    private $createdDate;
    /**
     * @var \DateTime $updated
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $verificationDate;

    // TODO anpassen
    public function __construct()
    {
        $this->emailAddress = '';

        $this->isVerified = false;
        $this->verificationToken = '';
        $this->isDataPolicyAccepted = false;
        $this->createdDate = new \DateTime();
        $this->verificationDate = new \DateTime();
    }

    public function toArray(): array
    {
        return [
            $this->id,
            $this->emailAddress,

            $this->isVerified === false ? 0 : 1,
            $this->verificationToken,
            $this->isDataPolicyAccepted === true ? 1 : 0,
            $this->createdDate instanceof \DateTime ? $this->createdDate->format('Y-m-d H:i:s') : '',
            $this->verificationDate instanceof \DateTime ? $this->verificationDate->format('Y-m-d H:i:s') : '',
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * @param string $emailAddress
     */
    public function setEmailAddress($emailAddress): void
    {
        $this->emailAddress = $emailAddress;
    }

    /**
     * @return bool
     */
    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    /**
     * @param bool $isVerified
     */
    public function setIsVerified(bool $isVerified): void
    {
        $this->isVerified = $isVerified;
    }

    /**
     * @return string
     */
    public function getVerificationToken()
    {
        return $this->verificationToken;
    }

    /**
     * @param string $verificationToken
     */
    public function setVerificationToken($verificationToken): void
    {
        $this->verificationToken = $verificationToken;
    }

    /**
     * @return bool
     */
    public function isDataPolicyAccepted(): bool
    {
        return $this->isDataPolicyAccepted;
    }

    /**
     * @param bool $isDataPolicyAccepted
     */
    public function setIsDataPolicyAccepted(bool $isDataPolicyAccepted): void
    {
        $this->isDataPolicyAccepted = $isDataPolicyAccepted;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate(): \DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     */
    public function setCreatedDate(\DateTime $createdDate): void
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return \DateTime
     */
    public function getVerificationDate(): \DateTime
    {
        return $this->verificationDate;
    }

    /**
     * @param \DateTime $verificationDate
     */
    public function setVerificationDate(\DateTime $verificationDate): void
    {
        $this->verificationDate = $verificationDate;
    }

    /**
     * Get first Name
     *
     * @return  string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set first Name
     *
     * @param  string  $firstName  First Name
     *
     * @return  self
     */
    public function setFirstName($firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }
    /**
     * @return  string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param  string  $lastName
     *
     * @return  self
     */
    public function setLastName($lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return  string
     */
    public function getCapacityOfSigning()
    {
        return $this->capacityOfSigning;
    }

    /**
     * @param  string  $capacityOfSigning
     *
     * @return  self
     */
    public function setCapacityOfSigning($capacityOfSigning)
    {
        $this->capacityOfSigning = $capacityOfSigning;

        return $this;
    }

    /**
     * @return  string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param  string  $title
     *
     * @return  self
     */
    public function setTitle($title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get currently I am a
     *
     * @return  string
     */
    public function getCurrentStatus()
    {
        return $this->currentStatus;
    }

    /**
     * Set currently I am a
     *
     * @param  string  $currentStatus  Currently I am a
     *
     * @return  self
     */
    public function setCurrentStatus($currentStatus)
    {
        $this->currentStatus = $currentStatus;

        return $this;
    }

    /**
     * Get currently I am a .. other
     *
     * @return  string
     */
    public function getCurrentStatusOther()
    {
        return $this->currentStatusOther;
    }

    /**
     * Set currently I am a .. other
     *
     * @param  string  $currentStatusOther  Currently I am a .. other
     *
     * @return  self
     */
    public function setCurrentStatusOther($currentStatusOther)
    {
        $this->currentStatusOther = $currentStatusOther;

        return $this;
    }

    /**
     * Get what is your area of study/work?
     *
     * @return  string
     */
    public function getAreaOfStudyWork()
    {
        return $this->areaOfStudyWork;
    }

    /**
     * Set what is your area of study/work?
     *
     * @param  string  $areaOfStudyWork  What is your area of study/work?
     *
     * @return  self
     */
    public function setAreaOfStudyWork($areaOfStudyWork)
    {
        $this->areaOfStudyWork = $areaOfStudyWork;

        return $this;
    }

    /**
     * Get what is your area of study/work?
     *
     * @return  string
     */
    public function getAreaOfStudyWorkOther()
    {
        return $this->areaOfStudyWorkOther;
    }

    /**
     * Set what is your area of study/work?
     *
     * @param  string  $areaOfStudyWorkOther  What is your area of study/work?
     *
     * @return  self
     */
    public function setAreaOfStudyWorkOther($areaOfStudyWorkOther)
    {
        $this->areaOfStudyWorkOther = $areaOfStudyWorkOther;

        return $this;
    }

    /**
     * Get Organization Name
     *
     * @return  string
     */
    public function getProfessionalOrganizationName()
    {
        return $this->professionalOrganizationName;
    }

    /**
     * Set Organization Name
     *
     * @param  string  $professionalOrganizationName  Organization Name
     *
     * @return  self
     */
    public function setProfessionalOrganizationName($professionalOrganizationName)
    {
        $this->professionalOrganizationName = $professionalOrganizationName;

        return $this;
    }

    /**
     * Get what term best describes your organization?
     *
     * @return  string
     */
    public function getProfessionalOrganizationDescription()
    {
        return $this->professionalOrganizationDescription;
    }

    /**
     * Set what term best describes your organization?
     *
     * @param  string  $professionalOrganizationDescription  What term best describes your organization?
     *
     * @return  self
     */
    public function setProfessionalOrganizationDescription($professionalOrganizationDescription)
    {
        $this->professionalOrganizationDescription = $professionalOrganizationDescription;

        return $this;
    }

    /**
     * Get what term best describes your organization?
     *
     * @return  string
     */
    public function getProfessionalOrganizationDescriptionOther()
    {
        return $this->professionalOrganizationDescriptionOther;
    }

    /**
     * Set what term best describes your organization?
     *
     * @param  string  $professionalOrganizationDescriptionOther  What term best describes your organization?
     *
     * @return  self
     */
    public function setProfessionalOrganizationDescriptionOther($professionalOrganizationDescriptionOther)
    {
        $this->professionalOrganizationDescriptionOther = $professionalOrganizationDescriptionOther;

        return $this;
    }

    /**
     * @return  string[]
     */
    public function getKindOfInvolvement(): array
    {
        return $this->kindOfInvolvement;
    }

    /**
     * Set would you like to be involved further with “Economists for Future”?
     *
     * @param  string[]  $kindOfInvolvement  Would you like to be involved further with “Economists for Future”?
     *
     * @return  self
     */
    public function setKindOfInvolvement(array $kindOfInvolvement)
    {
        $this->kindOfInvolvement = $kindOfInvolvement;

        return $this;
    }

    /**
     * Get city
     *
     * @return  string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set city
     *
     * @param  string  $city
     *
     * @return  self
     */
    public function setCity($city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get email Address
     *
     * @return  string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param  string  $country
     *
     * @return  self
     */
    public function setCountry($country): self
    {
        $this->country = $country;

        return $this;
    }
}
