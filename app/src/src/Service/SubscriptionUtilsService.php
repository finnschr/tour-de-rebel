<?php

namespace App\Service;

use App\Entity\Subscription;
use Twig\Environment;

class SubscriptionUtilsService
{
    private $twig;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    public function generateUuid()
    {
        return sprintf(
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0x0C2f) | 0x4000,
            mt_rand(0, 0x3fff) | 0x8000,
            mt_rand(0, 0x2Aff),
            mt_rand(0, 0xffD3),
            mt_rand(0, 0xff4B)
        );
    }

    public function sendVerificationMail(\Swift_Mailer $mailer, Subscription $subscription, string $token): void
    {
        $message = (new \Swift_Message('Verification for Tour de Planet Newsletter'))
            ->setFrom(['verify@tourdeplanet.org' => 'Tour de Planet'])
            ->setReplyTo(['no-reply@tourdeplanet.org' => 'Tour de Planet'])
            ->setTo($subscription->getEmailAddress())
            ->setBody($this->buildHTMLMailContent($subscription, $token), 'text/html')
            ->addPart($this->buildTextMailContent($subscription, $token), 'text/plain');
    
        $failedAddresses = [];
        $logger = new \Swift_Plugins_Loggers_ArrayLogger();
        $mailer->registerPlugin(new \Swift_Plugins_LoggerPlugin($logger));
        if (!$mailer->send($message, $failedAddresses)) {
            throw new \Exception($logger->dump());
        }
    }

    public function buildHTMLMailContent(Subscription $subscription, string $token): string
    {
        return $this->twig->render( // TODO Exception Handling?
            'pages/campaign/email_verification.html.twig',
            [
                'subscription' => $subscription,
                'token' => $token,
            ]
            );
    }

    public function buildTextMailContent(Subscription $subscription, string $token): string
    {
        return $this->twig->render( // TODO Exception Handling?
            'pages/campaign/email_verification.text.twig',
            [
                'subscription' => $subscription,
                'token' => $token,
            ]
            );
    }
}