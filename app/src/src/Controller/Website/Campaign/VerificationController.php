<?php

namespace App\Controller\Website\Campaign;

use App\Entity\Subscription;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class VerificationController extends AbstractController
{
    public function index(string $token, EntityManagerInterface $entityManager, LoggerInterface $logger): Response
    {
        /** @var Subscription $existingSubscription */
        $existingSubscription = $entityManager->getRepository(Subscription::class)
            ->findOneBy(['verificationToken' => $token]);

        if (!$existingSubscription instanceof Subscription) {
            $logger->warning('No subscription found to verify.');

            throw $this->createNotFoundException('No subscription found to verify.');
        }

        if ($existingSubscription->isVerified()) {
            $logger->warning('A second request to verify does not make sense.');

            throw $this->createNotFoundException('A second request to verify does not make sense.');
        }

        $existingSubscription->setIsVerified(true);
        $existingSubscription->setVerificationDate(new \DateTime('now'));
        $entityManager->persist($existingSubscription);
        $entityManager->flush();

        $logger->info('Successful subscription verification');

        return $this->render('pages/campaign/verification.html.twig', []);
    }
}
