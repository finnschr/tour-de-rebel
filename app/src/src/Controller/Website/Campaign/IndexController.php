<?php

namespace App\Controller\Website\Campaign;

use App\Entity\Subscription;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Service\SubscriptionUtilsService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class IndexController extends AbstractController
{
    public function index(
        Request $request,
        LoggerInterface $logger,
        EntityManagerInterface $entityManager,
        \Swift_Mailer $mailer,
        SubscriptionUtilsService $subscriptionUtils
    ): Response {
        $csrfToken = $request->request->get('token');
        $isValidToken = $this->isCsrfTokenValid('newsletter-mail', $csrfToken);
        if (!$isValidToken) {
            return $this->json(
                ['error' => []],
                Response::HTTP_BAD_REQUEST
            );
        }

        $emailAddress = $request->request->get('emailAddress');
        if (empty($emailAddress)) {
            return $this->json(['error' => ''], Response::HTTP_BAD_REQUEST);
        }

        $token = $subscriptionUtils->generateUuid();

        $logger->info('Verification token created: ' . $token);
        /** @var Subscription $subscription */
        $subscription = new Subscription();
        $subscription->setEmailAddress($emailAddress);
        $existingSubscription = $entityManager->getRepository(Subscription::class)
            ->findOneBy(['emailAddress' => $subscription->getEmailAddress()]);
        if ($existingSubscription instanceof Subscription) {
            $logger->warning('Second try with same email address');

            return $this->json(
                [
                    'error' => 'Mail-Address already exists: ' . $subscription->getEmailAddress(),
                ],
                Response::HTTP_BAD_REQUEST
            );
        }
        $subscription->setIsDataPolicyAccepted(false);
        $subscription->setVerificationToken($token);
        $subscription->setCreatedDate(new \DateTime('now'));
        $entityManager->persist($subscription);

        try {
            $result = $subscriptionUtils->sendVerificationMail($mailer, $subscription, $token);
        } catch (\Swift_TransportException $e) {
            $logger->error('Problems to send mail', ['message' => $e->getMessage()]);

            return $this->json(['error' => 'Problems to send Mail. Please try again.'], Response::HTTP_BAD_REQUEST);
        } catch (\Throwable $e) {
            $logger->error('Problems to send mail', ['message' => $e->getMessage()]);

            return $this->json(['error' => 'Problems to send Mail. Please try again.'], Response::HTTP_BAD_REQUEST);
        }
        $entityManager->flush();
        $logger->info('Successful subscription - Mail send');

        return $this->json(['success' => 'Successfully subscribed']);
    }
}
